
# coding: utf-8

# In[1]:


# Define access keys
access_key='AKIAJ4JKYBCTNC33EQLQ'    
secret='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'  
bucket_name='gowaii'


# In[2]:


# Define enviroment variables
import os  
os.environ['AWS_ACCESS_KEY_ID'] = access_key  
os.environ['AWS_SECRET_ACCESS_KEY'] = secret  
os.environ['PYSPARK_PYTHON'] = '/usr/bin/python2'  
os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.hadoop:hadoop-aws:2.7.1,com.amazonaws:aws-java-sdk-pom:1.10.34,com.databricks:spark-csv_2.11:1.3.0 pyspark-shell' 


# In[3]:


# Import pyspark and define SparkContext 

import pyspark  
sc = pyspark.SparkContext('local[*]')  
sqlContext = pyspark.SQLContext(sc)


# In[10]:


# Import necessary functions and packages

from pyspark.sql.functions import udf  
from pyspark.sql.functions import lit  
#from pyspark.sql import DataFrameReader
import boto3  
import boto
from datetime import time,date,datetime


# In[11]:


# Connect to s3

conn_s3 = boto.connect_s3()  
bucket = conn_s3.get_bucket(bucket_name)


# In[13]:


# List files in s3 

contents=bucket.list(prefix='')  
for f in contents:  
    print (f.name)
    print (f.size)
    selected_file = f.name
print ("\n\n--\nFile to process: %s" % selected_file )


# In[15]:


# Gests path from file

full_uri = "s3n://{}/{}".format(bucket_name, selected_file)  
print (full_uri) 


# In[18]:


# Download file
#csv
s3 = boto3.resource('s3')
s3.Bucket(bucket_name).download_file(selected_file, 'mb.csv')   # csv
marcas_blancas_df = sqlContext.read.load('mb.csv',  
                                  format='com.databricks.spark.csv',
                                  header='true',
                                  inferSchema='true',
                                  sep=';'      ) 
marcas_blancas_df.head(5)
 


# In[22]:


# Download file
#parquet
s3.Bucket(bucket_name).download_file(selected_file, 'mb.parquet')
marcas_blancas_df = sqlContext.read.parquet('mb.parquet') 
marcas_blancas_df.head(5)


# In[23]:


# Dataframe schema
marcas_blancas_df.printSchema()


# In[24]:


# Calculate actual date

now = datetime.today().strftime('%Y%m%d')


# In[26]:


# Create new path

path= 's3a://gowaii/raw/marcas_blancas/' + now + '/' 
print(path)


# In[28]:


# Set file name

name= now + '_' + now + '_marcas_blancas'
print(name)


# In[29]:


# Write file in parquet format

marcas_blancas_df.write.format('parquet').save(os.path.join(path, name),mode='overwrite')


# In[31]:


# Check uploaded file

contents=bucket.list(prefix='')  
for f in contents:  
    print (f.name)
    print (f.size)
    check_file = f.name
print ("\n\n--\nFile to process: %s" % check_file )
s3.Bucket(bucket_name).download_file(check_file, 'mb.parquet')
newDataDF = sqlContext.read.parquet('mb.parquet') 
newDataDF.head(5)


# In[ ]:




